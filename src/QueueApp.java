import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.annotation.Retention;
import java.util.LinkedList;
import java.util.jar.Attributes.Name;

import javax.swing.*;

public class QueueApp extends JFrame{
    JTextField txtName;
    JLabel lblQueuelist;
    JLabel lblCurrent;
    JButton btnAddQueue;
    JButton btnGetQueue;
    JButton btnClearQueue;
    JLabel picture;
    LinkedList<String> queue;
    public QueueApp(){
        super("Queue App");
        queue = new LinkedList();
        txtName = new JTextField();
        txtName.setBounds(30, 10, 200, 20);
        txtName.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                AddQueue();
            }});
        btnAddQueue = new JButton("Add Queue");
        btnAddQueue.setBounds(250,10,100,20);
        btnAddQueue.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                AddQueue();          
            }});

        btnGetQueue = new JButton("Get Queue");
        btnGetQueue.setBounds(250,40,100,20);
        btnGetQueue.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                getQueue();
            }});

        btnClearQueue = new JButton("Clear Queue");
        btnClearQueue.setBounds(250,70,110,20);
        btnClearQueue.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                ClearQueue();
            }});

        lblQueuelist = new JLabel("Empty");
        lblQueuelist.setBounds(30, 40, 200, 20);

        picture = new JLabel();
        picture.setBounds(120, 120, 200, 150);
        picture.setIcon(new ImageIcon("queue.png"));

        lblCurrent = new JLabel("?");
        lblCurrent.setHorizontalAlignment(JLabel.CENTER);
        lblCurrent.setFont(new Font("Serif",Font.PLAIN,50));
        lblCurrent.setBounds(30, 70, 200, 50);

        this.add(lblCurrent);
        this.add(picture);
        this.add(lblQueuelist);
        this.add(btnAddQueue);
        this.add(btnGetQueue);
        this.add(btnClearQueue);
        this.add(txtName);
        this.setSize(400,300);
        this.setLayout(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ShowQueue();
        this.setVisible(true);
    }
    public void ShowQueue(){
        if(queue.isEmpty()){
            lblQueuelist.setText("Empty");
        } else{
            lblQueuelist.setText(queue.toString());
        }
    }
    public void getQueue(){
        if(queue.isEmpty()){
            lblCurrent.setText("?");
            return;
        }
        String name = queue.remove();
        lblCurrent.setText(name);
        ShowQueue();
    }
    public void AddQueue(){
        String name = txtName.getText();
        if(name.equals("")){
            return;
        }
        queue.add(name);
        txtName.setText("");
        ShowQueue();
    }
    public void ClearQueue(){
        queue.clear();
        lblCurrent.setText("?");
        txtName.setText("");
        ShowQueue();
    }
    public static void main(String[] args) {
        QueueApp frame = new QueueApp();
    }
}
